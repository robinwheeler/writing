﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writing
{
    /// <summary>
    /// The <see cref="Pencil"/> class is used to control the text associated with a <see cref="Paper"/>
    /// </summary>
    public class Pencil
    {     
        #region Constructor
        /// <summary>
        /// Instantiates a <see cref="Pencil"/>  
        /// </summary>
        /// <param name="durability">Specifies the degree of loss of writing functionality that will be applied each time this <see cref="Pencil" writes on <see cref="Paper"/>/></param>
        public Pencil(int durability)
        {
            _durability = durability;
            _graphite = durability;
        }
        #endregion

        #region public methods

        #region WriteToPaper
        /// <summary>
        /// Appends text to the specified <see cref="Paper"/>
        /// </summary>
        /// <param name="paper"></param>
        public void WriteToPaper(Paper paper)
        {
            paper.WriteFromPencil(this);
        }
        #endregion
        #region SetText
        /// <summary>
        /// Sets the text that this <see cref="Pencil"/> will write.
        /// </summary>
        /// <param name="text"></param>
        public void SetText(string text)
        {
            _text = text;
        }
        #endregion
        #region Write
        /// <summary>
        /// Causes this <see cref="Pencil"/> to perform its writing function, which returns the text that it is currently 
        /// capable of creating per its current <see cref="Graphite"/> level, and reduces its graphite level accordingly.
        /// White space will not cause <see cref="Graphite"/> to be consumed.
        /// </summary>
        /// <returns>The text that it is currently capable of creating per its current <see cref="Graphite"/> level</returns>
        public string Write()
        {
            StringBuilder retBuilder = new StringBuilder();

            foreach (char _text in _text)
            {
                if (_graphite <= 0)
                {
                    retBuilder.Append(" ");
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(_text.ToString()))
                    {
                        if (char.IsUpper(_text))
                            _graphite = _graphite - 2;
                        else
                            _graphite--;
                    }
                    retBuilder.Append(_text);
                }
            }

            return retBuilder.ToString();
        }
        #endregion

        #region Sharpen
        /// <summary>
        /// Resets the <see cref="_graphite"/> level to the original durability rating.
        /// </summary>
        public void Sharpen()
        {
            _graphite = _durability;
        }
        #endregion

        #endregion

        #region fields
        private string _text;
        private int _graphite;
        private int _durability;
        #endregion
    }
}
