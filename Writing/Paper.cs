﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writing
{
    /// <summary>
    /// The <see cref="Paper"/> class models the behavior of a conceptual piece of paper as it interacts with a <see cref="Pencil"/>
    /// </summary>
    public class Paper
    {
        #region public methods

        #region WriteFromPencil
        /// <summary>
        /// Sets <see cref="Text"/> according to the specified <see cref="Pencil"/>
        /// </summary>
        /// <param name="pencil"></param>
        public void WriteFromPencil(Pencil pencil)
        {
            Text += pencil.Write();
        }
        #endregion

        #endregion

        #region properties
        public string Text { get; private set; }
        #endregion
    }
}
