﻿using System;
using System.Text;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Writing;

namespace WritingTests
{
    [TestClass]
    public class PencilTests
    {
        [TestMethod]
        public void Write_Should_AppendTextToPaper()
        {
            var pencil = new Pencil(testTextWithoutSpaces.Length * 2);
            var paper = new Paper();

            pencil.SetText(testTextWithoutSpaces);
            pencil.WriteToPaper(paper);

            Assert.AreEqual(testTextWithoutSpaces, paper.Text);

            pencil.WriteToPaper(paper);

            Assert.AreEqual(testTextWithoutSpaces + testTextWithoutSpaces, paper.Text);
        }
        [TestMethod]
        public void Write_Should_AppendSpaceWhenGraphiteEqualsZero()
        {
            var pencil = new Pencil(0);
            var paper = new Paper();

            pencil.SetText(testTextWithoutSpaces);
            pencil.WriteToPaper(paper);

            Assert.AreEqual(MakeIntoSpaces(testTextWithoutSpaces), paper.Text);
        }
        [TestMethod]
        public void Write_Should_DegradeGraphitePerCharacterWritten()
        {
            var pencil = new Pencil(5);
            var paper = new Paper();

            pencil.SetText(testTextWithoutSpaces);
            pencil.WriteToPaper(paper);

            Assert.AreEqual(testTextWithoutSpaces.Substring(0, 5) + MakeIntoSpaces(testTextWithoutSpaces.Substring(5, testTextWithoutSpaces.Length - 5)), paper.Text);
        }
        [TestMethod]
        public void Write_ShouldNot_DegradeGraphiteForWhitespcace()
        {
            var pencil = new Pencil(testTextWithSpaces.Replace(" ", "").Length);
            var paper = new Paper();

            pencil.SetText(testTextWithSpaces);
            pencil.WriteToPaper(paper);

            Assert.AreEqual(testTextWithSpaces, paper.Text);
        }
        [TestMethod]
        public void Write_Should_DegradeTwoLevelsForCapitals()
        {

            var capCount = testTextWithCapitals.Where(l => char.IsUpper(l)).Count();
            var lowerCount = testTextWithCapitals.Where(l => char.IsLower(l)).Count();
            var capDegredation = capCount * 2;
            var lowerDegredation = lowerCount;
            var anticipatedDurability = testTextWithCapitals.Length - capDegredation;

            var pencil = new Pencil(capDegredation + lowerDegredation);
            var paper = new Paper();

            pencil.SetText(testTextWithCapitals);
            pencil.WriteToPaper(paper);

            // first write should get full test
            Assert.AreEqual(testTextWithCapitals, paper.Text);

            // second write should get only spaces
            pencil.WriteToPaper(paper);
            Assert.AreEqual(testTextWithCapitals + MakeIntoSpaces(testTextWithCapitals), paper.Text);
        }
        [TestMethod]
        public void Sharpen_Should_ResetGraphiteLevelToDurability()
        {

            var pencil = new Pencil(testTextWithoutSpaces.Length);
            var paper = new Paper();

            pencil.SetText(testTextWithoutSpaces);
            pencil.WriteToPaper(paper);

            // first write should get full text
            Assert.AreEqual(testTextWithoutSpaces, paper.Text);

            // second write should get only spaces
            pencil.WriteToPaper(paper);
            Assert.AreEqual(testTextWithoutSpaces + MakeIntoSpaces(testTextWithoutSpaces), paper.Text);

            pencil.Sharpen();

            // third write should get text again
            pencil.WriteToPaper(paper);
            Assert.AreEqual(testTextWithoutSpaces + MakeIntoSpaces(testTextWithoutSpaces) + testTextWithoutSpaces, paper.Text);
        }

        private string MakeIntoSpaces(string str)
        {
            StringBuilder retBuilder = new StringBuilder();

            for (int i = 0; i < str.Length; i++)
            {
                retBuilder.Append(" ");
            }

            return retBuilder.ToString();
        }

        private const string testTextWithoutSpaces = "shesellsseashellsdownbytheseashore";
        private const string testTextWithSpaces = "she sells sea shells down by the sea shore";
        private const string testTextWithCapitals = "SheSellsSeaShellsDownByTheSeaShore";
    }
}
