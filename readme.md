﻿The Writing solution consist of a library project (Writing) providing the 
Pencil and Paper classes and a test project (WritingTest) providing the PencilTests class.

The Pencil and Paper classes model the behavior of a pencil creating writing on a paper including an 
approximation of the degradation of the writing medium as characters are created.

The PencilTests class is used to verify funtionality of the user stories found in the product documentation.

Prerequisites
	.NET Framework 4.6.2

Use
	Add a reference to Writing.dll to your project in order to access its functionality.

Testing
	All tests in the WritingTests project can be run in a local environment.

